A simple C# WinForms application that I did before I went to college.
It can store some notes in a SQLite database and remind me about them. The application can be closed to the System Tray.

Technologies used : 

* C#
* SQLite