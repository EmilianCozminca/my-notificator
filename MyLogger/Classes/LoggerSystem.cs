﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLogger.Classes
{
    class LoggerSystem :IDisposable
    {
        public SQLHandler o_SQLManager;
        public TrayManager o_Notifyer;

        public LoggerSystem() { }

        public void Start()
        {
            o_SQLManager = new SQLHandler();
            o_Notifyer = new TrayManager(this);
        }

        public void Dispose()
        {
            if (o_SQLManager != null) { o_SQLManager.Dispose(); o_SQLManager = null; }
            if (o_Notifyer != null) { o_Notifyer.Dispose(); o_Notifyer = null; }
        }
    }
}
