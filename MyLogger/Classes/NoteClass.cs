﻿/* Emy
 * Stocheaza obiectele citide din XML
 */

using System;

namespace MyLogger.Classes
{
    public class NoteClass
    {
        public NoteClass()
        {
            v_ID = Guid.NewGuid().ToString("N"); //ID unic 

            v_Tip= "Nota";
            v_Nume= "Nume";
            v_ModRepetare= "Zilnic";
            v_Interes= "Mic";
            v_Descriere= "";

            v_NumarRepetari = 0;

            v_Lansare = DateTime.Now.Date;

            v_Repetare = false;
            v_UltimaNotif = DateTime.Now.Date.AddDays(-1);
        }

        public string v_ID { get; set; }
        public string v_Tip { get; set; }
        public string v_Nume { get; set; }
        public DateTime v_Lansare { get; set; }
        public bool v_Repetare { get; set; }
        public string v_ModRepetare { get; set;}
        public Int16 v_NumarRepetari { get; set; }
        public string v_Interes { get; set; }
        public string v_Descriere { get; set; }
        public DateTime v_UltimaNotif { get; set; }
    }
}
