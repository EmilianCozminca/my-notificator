﻿using System;
using System.Windows.Forms;
using MyLogger.Classes;
using System.Collections.Generic;
using System.Linq;

namespace MyLogger
{
    partial class FormularGridView : Form
    {
        LoggerSystem passed_LS = null;
        List<NoteClass> dataList;
        List<string> toDelete = null;
            
        //Singleton
        private static FormularGridView openForm = null;
        public static FormularGridView GetInstance(LoggerSystem p_LS)
        {
            if (openForm == null)
            {
                openForm = new FormularGridView(p_LS);
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        public FormularGridView(LoggerSystem p_LS)
        {        
            InitializeComponent();
            passed_LS = p_LS;

            //v_RepetareCheckBox.Checked = true;

            dataList = passed_LS.o_SQLManager.Select();
            toDelete = new List<string>();       

            noteClassBindingSource.DataSource = dataList;
        }

        //private void toggleDelete_Click(object sender, EventArgs e)
        //{
        //    if (noteClassBindingSource.Current != null)
        //    {
        //        NoteClass nc = (NoteClass)noteClassBindingSource.Current;

        //        if (nc.v_MarchatStergere)
        //            nc.v_MarchatStergere = false;
        //        else
        //            nc.v_MarchatStergere = true;

        //        toDelete.Add((NoteClass)noteClassBindingSource.Current);

        //        noteClassDataGridView.Refresh();
        //    }
        //}
        private void aplicaStergere_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell oneCell in noteClassDataGridView.SelectedCells)
            {
                if (oneCell.Selected)
                {
                    NoteClass x = (NoteClass) noteClassBindingSource.Current;
                    {
                        toDelete.Add(x.v_ID);
                    }
                    noteClassDataGridView.Rows.RemoveAt(oneCell.RowIndex);
                }
            }                
            noteClassDataGridView.Refresh();
        }
        
        
        
        private void noteClassBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            passed_LS.o_SQLManager.Insert(dataList);
            foreach(string ncl in toDelete)
                passed_LS.o_SQLManager.Delete(ncl);

            //dataList = passed_LS.o_SQLManager.Select();
            //noteClassDataGridView.Refresh();
        }



        private void noteClassDataGridView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            NoteClass NCL = (NoteClass)noteClassBindingSource.Current;
            var x = new Forms.FormularEditare(NCL);
            x.ShowDialog();

            noteClassDataGridView.Refresh();
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}