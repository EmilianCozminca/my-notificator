﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using MyLogger.Classes;

namespace MyLogger
{
    partial class FormularOptiuni : Form
    {
        LoggerSystem passed_LS = null;

        //Singleton (Don't want multiple options forms)
        private static FormularOptiuni openForm = null;
        public static FormularOptiuni GetInstance(LoggerSystem p_LS)
        {
            if (openForm == null)
            {
                openForm = new FormularOptiuni(p_LS);
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }
        //~Singleton

        //Registry Key, shared between Initialization and SaveBtn
        RegistryKey rkStart = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public FormularOptiuni(LoggerSystem p_LS)
        {       
            InitializeComponent();
            passed_LS = p_LS;

            if (rkStart.GetValue("MyLogger") == null)
                CB_winStart.Checked = false;
            else
                CB_winStart.Checked = true;

            //Interval
            TB_IntRep.Text = Properties.Settings.Default.RepeatInterval.ToString();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            //Saving Timer Repeat Interval
            if (Properties.Settings.Default.RepeatInterval != Convert.ToInt16(TB_IntRep.Text))  //Prevent timer reset ?
            {
                Properties.Settings.Default.RepeatInterval = Convert.ToInt16(TB_IntRep.Text);   //We save the info to Properties
                Properties.Settings.Default.Save(); //We save the settings in application folder in AppData (Handled by .NET)

                passed_LS.o_Notifyer.setRepIntervalFromSettings();  //We apply the interval to Timer;; rereads the settings we just applyed
            }

            //Saving registry setting
            if(CB_winStart.Checked == true) 
                rkStart.SetValue("MyLogger", Application.ExecutablePath.ToString());
            else 
                rkStart.DeleteValue("MyLogger", false);
            
            this.Close();
        }
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
