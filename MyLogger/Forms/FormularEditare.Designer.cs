﻿namespace MyLogger.Forms
{
    partial class FormularEditare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelID = new System.Windows.Forms.Label();
            this.NotificareChB = new System.Windows.Forms.CheckBox();
            this.SalvBtn = new System.Windows.Forms.Button();
            this.revocBtn = new System.Windows.Forms.Button();
            this.ModRepetareCB = new System.Windows.Forms.ComboBox();
            this.InteresCB = new System.Windows.Forms.ComboBox();
            this.DescriereRTB = new System.Windows.Forms.RichTextBox();
            this.NrNotTB = new System.Windows.Forms.TextBox();
            this.TIPCB = new System.Windows.Forms.ComboBox();
            this.NumeTB = new System.Windows.Forms.TextBox();
            this.LansareDTP = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "NUME";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "TIP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "LANSARE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "NOTIFICARE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "NUMAR NOTIFICARI";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "MOD REPETARE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "INTERES";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "DESCRIERE";
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(191, 9);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 1;
            this.labelID.Text = "ID";
            // 
            // NotificareChB
            // 
            this.NotificareChB.AutoSize = true;
            this.NotificareChB.Location = new System.Drawing.Point(194, 105);
            this.NotificareChB.Name = "NotificareChB";
            this.NotificareChB.Size = new System.Drawing.Size(15, 14);
            this.NotificareChB.TabIndex = 2;
            this.NotificareChB.UseVisualStyleBackColor = true;
            this.NotificareChB.CheckedChanged += new System.EventHandler(this.NotificareChB_CheckedChanged);
            // 
            // SalvBtn
            // 
            this.SalvBtn.Location = new System.Drawing.Point(238, 370);
            this.SalvBtn.Name = "SalvBtn";
            this.SalvBtn.Size = new System.Drawing.Size(75, 23);
            this.SalvBtn.TabIndex = 3;
            this.SalvBtn.Text = "Salvare";
            this.SalvBtn.UseVisualStyleBackColor = true;
            this.SalvBtn.Click += new System.EventHandler(this.SalvBtn_Click);
            // 
            // revocBtn
            // 
            this.revocBtn.Location = new System.Drawing.Point(336, 370);
            this.revocBtn.Name = "revocBtn";
            this.revocBtn.Size = new System.Drawing.Size(75, 23);
            this.revocBtn.TabIndex = 3;
            this.revocBtn.Text = "Revocare";
            this.revocBtn.UseVisualStyleBackColor = true;
            this.revocBtn.Click += new System.EventHandler(this.revocBtn_Click);
            // 
            // ModRepetareCB
            // 
            this.ModRepetareCB.FormattingEnabled = true;
            this.ModRepetareCB.Items.AddRange(new object[] {
            "Zilnic",
            "Saptamanal",
            "Lunar"});
            this.ModRepetareCB.Location = new System.Drawing.Point(194, 154);
            this.ModRepetareCB.Name = "ModRepetareCB";
            this.ModRepetareCB.Size = new System.Drawing.Size(121, 21);
            this.ModRepetareCB.TabIndex = 4;
            // 
            // InteresCB
            // 
            this.InteresCB.FormattingEnabled = true;
            this.InteresCB.Items.AddRange(new object[] {
            "Mic",
            "Mediu",
            "Mare"});
            this.InteresCB.Location = new System.Drawing.Point(194, 181);
            this.InteresCB.Name = "InteresCB";
            this.InteresCB.Size = new System.Drawing.Size(121, 21);
            this.InteresCB.TabIndex = 4;
            // 
            // DescriereRTB
            // 
            this.DescriereRTB.Location = new System.Drawing.Point(15, 258);
            this.DescriereRTB.Name = "DescriereRTB";
            this.DescriereRTB.Size = new System.Drawing.Size(396, 106);
            this.DescriereRTB.TabIndex = 5;
            this.DescriereRTB.Text = "";
            // 
            // NrNotTB
            // 
            this.NrNotTB.Location = new System.Drawing.Point(194, 128);
            this.NrNotTB.Name = "NrNotTB";
            this.NrNotTB.Size = new System.Drawing.Size(121, 20);
            this.NrNotTB.TabIndex = 6;
            // 
            // TIPCB
            // 
            this.TIPCB.FormattingEnabled = true;
            this.TIPCB.Items.AddRange(new object[] {
            "Nota",
            "Serial",
            "Joc",
            "Eveniment"});
            this.TIPCB.Location = new System.Drawing.Point(194, 51);
            this.TIPCB.Name = "TIPCB";
            this.TIPCB.Size = new System.Drawing.Size(121, 21);
            this.TIPCB.TabIndex = 7;
            // 
            // NumeTB
            // 
            this.NumeTB.Location = new System.Drawing.Point(194, 25);
            this.NumeTB.Name = "NumeTB";
            this.NumeTB.Size = new System.Drawing.Size(121, 20);
            this.NumeTB.TabIndex = 8;
            // 
            // LansareDTP
            // 
            this.LansareDTP.Location = new System.Drawing.Point(194, 79);
            this.LansareDTP.Name = "LansareDTP";
            this.LansareDTP.Size = new System.Drawing.Size(121, 20);
            this.LansareDTP.TabIndex = 9;
            // 
            // FormularEditare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 405);
            this.ControlBox = false;
            this.Controls.Add(this.LansareDTP);
            this.Controls.Add(this.NumeTB);
            this.Controls.Add(this.TIPCB);
            this.Controls.Add(this.NrNotTB);
            this.Controls.Add(this.DescriereRTB);
            this.Controls.Add(this.InteresCB);
            this.Controls.Add(this.ModRepetareCB);
            this.Controls.Add(this.revocBtn);
            this.Controls.Add(this.SalvBtn);
            this.Controls.Add(this.NotificareChB);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormularEditare";
            this.Text = "FormularEditare";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox NotificareChB;
        private System.Windows.Forms.Button SalvBtn;
        private System.Windows.Forms.Button revocBtn;
        private System.Windows.Forms.ComboBox ModRepetareCB;
        private System.Windows.Forms.ComboBox InteresCB;
        private System.Windows.Forms.RichTextBox DescriereRTB;
        private System.Windows.Forms.TextBox NrNotTB;
        private System.Windows.Forms.ComboBox TIPCB;
        private System.Windows.Forms.TextBox NumeTB;
        private System.Windows.Forms.DateTimePicker LansareDTP;
        private System.Windows.Forms.Label labelID;
    }
}