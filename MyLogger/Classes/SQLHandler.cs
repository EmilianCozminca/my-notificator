﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace MyLogger.Classes
{
    class SQLHandler : IDisposable
    {
        SQLiteConnection m_dbConnection;
        
        public SQLHandler()
        {
            //SQLiteConnection.CreateFile("MyLoggerDatabase.sqlite");

            m_dbConnection = new SQLiteConnection("Data Source=MyLoggerDatabase.sqlite;Version=3;");
            m_dbConnection.Open();

            string sql = "CREATE TABLE IF NOT EXISTS tableData (ID TEXT PRIMARY KEY NOT NULL," +
                                                   "TIP TEXT NOT NULL, " +
                                                   "NUME TEXT NOT NULL, " +
                                                   "LANSARE TEXT, " +
                                                   "REPETARE INT, " +
                                                   "MODREPETARE TEXT, " +
                                                   "NUMARREPETARI INT, " +
                                                   "INTERES TEXT, " +
                                                   "DESCRIERE TEXT, " +
                                                   "ULTIMANOTIF TEXT)";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public List<NoteClass> Select(string condition = "ID <> 0")
        {
            List<NoteClass> tempNC = new List<NoteClass>();

            string sql = "select * from tableDATA WHERE " + condition ;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                NoteClass tNC = new NoteClass();
                tNC.v_ID = reader["ID"].ToString();
                tNC.v_Tip = reader["TIP"].ToString();
                tNC.v_Nume = reader["NUME"].ToString();
                tNC.v_Lansare = DateTime.Parse(reader["LANSARE"].ToString());
                tNC.v_Repetare = reader["REPETARE"].ToString().Equals("1");
                tNC.v_ModRepetare = reader["MODREPETARE"].ToString();
                tNC.v_NumarRepetari = Convert.ToInt16(reader["NUMARREPETARI"].ToString());
                tNC.v_Interes = reader["INTERES"].ToString();
                tNC.v_Descriere = reader["DESCRIERE"].ToString();
                tNC.v_UltimaNotif = DateTime.Parse(reader["ULTIMANOTIF"].ToString());

                tempNC.Add(tNC);
            }
            return tempNC;
        }
        public void Insert(List<NoteClass> tempNC)
        {
            foreach (NoteClass x in tempNC)
            {
                string sql = "INSERT OR REPLACE INTO tableData (ID," +
                                                       "TIP, " +
                                                       "NUME, " +
                                                       "LANSARE, " +
                                                       "REPETARE, " +
                                                       "MODREPETARE, " +
                                                       "NUMARREPETARI, " +
                                                       "INTERES, " +
                                                       "DESCRIERE, " +
                                                       "ULTIMANOTIF)" +
                                                       "VALUES (@ID, @TIP, @NUME, @LANSARE, @REPETARE, @MODREPETARE, @NUMARREPETARI, @INTERES, @DESCRIERE, @ULTIMANOTIF)";

                var command = new SQLiteCommand(sql, m_dbConnection);
                command.Parameters.AddWithValue("@ID" , x.v_ID);
                command.Parameters.AddWithValue("@TIP", x.v_Tip);
                command.Parameters.AddWithValue("@NUME", x.v_Nume);
                command.Parameters.AddWithValue("@LANSARE", x.v_Lansare.ToString("yyyy-MM-dd"));
                command.Parameters.AddWithValue("@REPETARE", x.v_Repetare);
                command.Parameters.AddWithValue("@MODREPETARE", x.v_ModRepetare);
                command.Parameters.AddWithValue("@NUMARREPETARI", x.v_NumarRepetari);
                command.Parameters.AddWithValue("@INTERES", x.v_Interes);
                command.Parameters.AddWithValue("@DESCRIERE", x.v_Descriere);
                command.Parameters.AddWithValue("@ULTIMANOTIF", x.v_UltimaNotif);

                command.ExecuteNonQuery();
            }
        }
        public void Delete(String ID)
        {
            string sql = "DELETE FROM tableData where ID = @ID";

            var command = new SQLiteCommand(sql, m_dbConnection);
            command.Parameters.AddWithValue("@ID", ID);

            command.ExecuteNonQuery();
        }
        public void Dispose()
        {
            if (m_dbConnection != null) { m_dbConnection.Close(); m_dbConnection.Dispose(); m_dbConnection = null; }
        }
    }
}
