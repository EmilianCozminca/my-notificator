﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyLogger.Classes;

namespace MyLogger.Forms
{
    public partial class FormularEditare : Form
    {
        private NoteClass workingNC;

        public FormularEditare(NoteClass pLC)
        {
            InitializeComponent();
            workingNC = pLC;

            labelID.Text = pLC.v_ID;
            NumeTB.Text = pLC.v_Nume;
            TIPCB.Text = pLC.v_Tip;
            LansareDTP.Value = pLC.v_Lansare;
            NotificareChB.Checked = pLC.v_Repetare;
            NrNotTB.Text = pLC.v_NumarRepetari.ToString();
            ModRepetareCB.Text = pLC.v_ModRepetare;
            InteresCB.Text = pLC.v_Interes;
            DescriereRTB.Text = pLC.v_Descriere;

            if (NotificareChB.Checked)
            {
                ModRepetareCB.Enabled = true;
                NrNotTB.Enabled = true;
            }
            else
            {
                ModRepetareCB.Enabled = false;
                NrNotTB.Enabled = false;
            }
        }

        private void SalvBtn_Click(object sender, EventArgs e)
        {
            workingNC.v_Nume = NumeTB.Text;
            workingNC.v_Tip = TIPCB.Text;
            workingNC.v_Lansare = LansareDTP.Value;
            workingNC.v_Repetare = NotificareChB.Checked;
            workingNC.v_ModRepetare = ModRepetareCB.Text;
            workingNC.v_Interes = InteresCB.Text;
            workingNC.v_Descriere = DescriereRTB.Text;

            int n;
            if (int.TryParse(NrNotTB.Text, out n))
            {
                workingNC.v_NumarRepetari = (Int16)n;

                Close();
            }
            else
            {
                MessageBox.Show("Valoarea repetarilor trebuie sa fie un numar!");
                workingNC.v_NumarRepetari = 0;
                Close();
            }
        }
        private void revocBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void NotificareChB_CheckedChanged(object sender, EventArgs e)
        {
            if (NotificareChB.Checked)
            {
                ModRepetareCB.Enabled = true;
                NrNotTB.Enabled = true;
            }
            else
            {
                ModRepetareCB.Enabled = false;
                NrNotTB.Enabled = false;
            }
        }
    }
}
