﻿namespace MyLogger
{
    partial class FormularGridView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormularGridView));
            this.noteClassBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.noteClassBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aplicaStergere = new System.Windows.Forms.ToolStripButton();
            this.noteClassBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.closeBtn = new System.Windows.Forms.ToolStripButton();
            this.noteClassDataGridView = new System.Windows.Forms.DataGridView();
            this.v_Tip = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.v_ModRepetare = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.v_Interes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.v_UltimaNotif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.noteClassBindingNavigator)).BeginInit();
            this.noteClassBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.noteClassBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteClassDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // noteClassBindingNavigator
            // 
            this.noteClassBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.noteClassBindingNavigator.BindingSource = this.noteClassBindingSource;
            this.noteClassBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.noteClassBindingNavigator.DeleteItem = null;
            this.noteClassBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.aplicaStergere,
            this.noteClassBindingNavigatorSaveItem,
            this.closeBtn});
            this.noteClassBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.noteClassBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.noteClassBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.noteClassBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.noteClassBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.noteClassBindingNavigator.Name = "noteClassBindingNavigator";
            this.noteClassBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.noteClassBindingNavigator.Size = new System.Drawing.Size(952, 25);
            this.noteClassBindingNavigator.TabIndex = 22;
            this.noteClassBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = global::MyLogger.Properties.Resources.Add;
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // noteClassBindingSource
            // 
            this.noteClassBindingSource.DataSource = typeof(MyLogger.Classes.NoteClass);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = global::MyLogger.Properties.Resources.previous;
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = global::MyLogger.Properties.Resources.next;
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // aplicaStergere
            // 
            this.aplicaStergere.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.aplicaStergere.Image = global::MyLogger.Properties.Resources.delete;
            this.aplicaStergere.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.aplicaStergere.Name = "aplicaStergere";
            this.aplicaStergere.Size = new System.Drawing.Size(23, 22);
            this.aplicaStergere.ToolTipText = "Aplica Stergere";
            this.aplicaStergere.Click += new System.EventHandler(this.aplicaStergere_Click);
            // 
            // noteClassBindingNavigatorSaveItem
            // 
            this.noteClassBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.noteClassBindingNavigatorSaveItem.Image = global::MyLogger.Properties.Resources.stiffy;
            this.noteClassBindingNavigatorSaveItem.Name = "noteClassBindingNavigatorSaveItem";
            this.noteClassBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.noteClassBindingNavigatorSaveItem.Text = "Save Data";
            this.noteClassBindingNavigatorSaveItem.Click += new System.EventHandler(this.noteClassBindingNavigatorSaveItem_Click);
            // 
            // closeBtn
            // 
            this.closeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.closeBtn.Image = global::MyLogger.Properties.Resources._in;
            this.closeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(23, 22);
            this.closeBtn.ToolTipText = "Inchide";
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // noteClassDataGridView
            // 
            this.noteClassDataGridView.AllowUserToAddRows = false;
            this.noteClassDataGridView.AllowUserToDeleteRows = false;
            this.noteClassDataGridView.AllowUserToOrderColumns = true;
            this.noteClassDataGridView.AutoGenerateColumns = false;
            this.noteClassDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.noteClassDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.v_Tip,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn6,
            this.v_ModRepetare,
            this.v_Interes,
            this.v_UltimaNotif});
            this.noteClassDataGridView.DataSource = this.noteClassBindingSource;
            this.noteClassDataGridView.Location = new System.Drawing.Point(12, 28);
            this.noteClassDataGridView.Name = "noteClassDataGridView";
            this.noteClassDataGridView.ReadOnly = true;
            this.noteClassDataGridView.Size = new System.Drawing.Size(944, 354);
            this.noteClassDataGridView.TabIndex = 22;
            this.noteClassDataGridView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.noteClassDataGridView_MouseDoubleClick);
            // 
            // v_Tip
            // 
            this.v_Tip.DataPropertyName = "v_Tip";
            this.v_Tip.HeaderText = "Tip";
            this.v_Tip.Items.AddRange(new object[] {
            "Nota",
            "Serial",
            "Joc",
            "Eveniment"});
            this.v_Tip.Name = "v_Tip";
            this.v_Tip.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "v_Nume";
            this.dataGridViewTextBoxColumn3.HeaderText = "Nume";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "v_Descriere";
            this.dataGridViewTextBoxColumn8.HeaderText = "Descriere";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "v_Lansare";
            this.dataGridViewTextBoxColumn4.HeaderText = "Lansare";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "v_Repetare";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Notificare";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "v_NumarRepetari";
            this.dataGridViewTextBoxColumn6.HeaderText = "Numar Repetari";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // v_ModRepetare
            // 
            this.v_ModRepetare.DataPropertyName = "v_ModRepetare";
            this.v_ModRepetare.HeaderText = "Mod Repetare";
            this.v_ModRepetare.Items.AddRange(new object[] {
            "Zilnic",
            "Saptamanal",
            "Lunar"});
            this.v_ModRepetare.Name = "v_ModRepetare";
            this.v_ModRepetare.ReadOnly = true;
            // 
            // v_Interes
            // 
            this.v_Interes.DataPropertyName = "v_Interes";
            this.v_Interes.HeaderText = "Interes";
            this.v_Interes.Items.AddRange(new object[] {
            "Mic",
            "Mediu",
            "Mare"});
            this.v_Interes.Name = "v_Interes";
            this.v_Interes.ReadOnly = true;
            // 
            // v_UltimaNotif
            // 
            this.v_UltimaNotif.DataPropertyName = "v_UltimaNotif";
            this.v_UltimaNotif.HeaderText = "Ultima Notificare";
            this.v_UltimaNotif.Name = "v_UltimaNotif";
            this.v_UltimaNotif.ReadOnly = true;
            // 
            // FormularGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 378);
            this.ControlBox = false;
            this.Controls.Add(this.noteClassDataGridView);
            this.Controls.Add(this.noteClassBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormularGridView";
            this.ShowIcon = false;
            this.Text = "MyLogger";
            ((System.ComponentModel.ISupportInitialize)(this.noteClassBindingNavigator)).EndInit();
            this.noteClassBindingNavigator.ResumeLayout(false);
            this.noteClassBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.noteClassBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noteClassDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource noteClassBindingSource;
        private System.Windows.Forms.BindingNavigator noteClassBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton noteClassBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView noteClassDataGridView;
        private System.Windows.Forms.ToolStripButton aplicaStergere;
        private System.Windows.Forms.ToolStripButton closeBtn;
        //private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn v_Tip;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn v_ModRepetare;
        private System.Windows.Forms.DataGridViewComboBoxColumn v_Interes;
        private System.Windows.Forms.DataGridViewTextBoxColumn v_UltimaNotif;





    }
}