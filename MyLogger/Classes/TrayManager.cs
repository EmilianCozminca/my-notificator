﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLogger.Classes
{
    class TrayManager : IDisposable
    {
        LoggerSystem passed_LSystem;

        Timer o_Timer;
        NotifyIcon o_NotifyIcon;
        ContextMenuStrip o_ContextMenu;

        public TrayManager(LoggerSystem tmp_PassedSystem)
        {
            passed_LSystem = tmp_PassedSystem;

            o_Timer = new Timer();
            o_Timer.Tick += new EventHandler(timer_Tick);


            o_ContextMenu = new ContextMenuStrip();

            setRepIntervalFromSettings(); // Citeste intervalul inainte de pornirea cronometrului
            o_Timer.Enabled = true;
            o_Timer.Start();

            var o_menu = new ToolStripMenuItem("Meniu");
            o_menu.Image = Properties.Resources.browser;
            o_menu.Click += this.OnMenuClicked;

            var o_notif = new ToolStripMenuItem("Notificari");
            o_notif.Image = Properties.Resources.eye;
            o_notif.Click += this.OnTodayNotesClicked;

            var o_options = new ToolStripMenuItem("Optiuni");
            o_options.Image = Properties.Resources.settings;
            o_options.Click += this.OnOptionsClicked;

            var o_exit = new ToolStripMenuItem("Inchide");
            o_exit.Image = Properties.Resources._in;
            o_exit.Click += OnExitClick;

            o_ContextMenu.Items.Add(o_menu);
            o_ContextMenu.Items.Add(o_notif);
            o_ContextMenu.Items.Add(o_options);
            o_ContextMenu.Items.Add(o_exit);

            o_NotifyIcon = new NotifyIcon();
            o_NotifyIcon.Icon = Properties.Resources.calendar;
            o_NotifyIcon.Visible = true;
            o_NotifyIcon.Text = "My Logger";
            o_NotifyIcon.BalloonTipClicked += (new EventHandler(this.OnTodayNotesClicked));
            o_NotifyIcon.DoubleClick += (new EventHandler(this.OnMenuClicked));
            o_NotifyIcon.ContextMenuStrip = o_ContextMenu;
        }
        public void setRepIntervalFromSettings()
        {
            o_Timer.Interval = Properties.Settings.Default.RepeatInterval * 60 * 1000;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            List<NoteClass> nc = passed_LSystem.o_SQLManager.Select("REPETARE == '1' " +
                                                                        " AND NUMARREPETARI > 0" + //Don't want it to be displayed there but want it to be disabled in showTodayNotes
                                                                        " AND LANSARE <= date('now')" +
                                                                        " AND ULTIMANOTIF <  date('now')");
            if (nc.Count > 0)
            {
                String noteNames = null;
                foreach (NoteClass o_nc in nc)
                {
                    noteNames += o_nc.v_Nume + "\n";
                }

                o_NotifyIcon.BalloonTipTitle = "Aveți notificari";
                o_NotifyIcon.BalloonTipText = noteNames;
                o_NotifyIcon.ShowBalloonTip(500);
            }
        }
        private void showTodayNotes()
        {
            List<NoteClass> nc = passed_LSystem.o_SQLManager.Select("REPETARE == '1' " +
                                                                        " AND LANSARE <= date('now')" +
                                                                        " AND ULTIMANOTIF < date('now')");
            if (nc.Count > 0)
            {
                foreach (NoteClass o_nc in nc)
                {
                    if (o_nc.v_NumarRepetari > 0)
                    {
                        if (MessageBox.Show(o_nc.v_Descriere + "\n Confirmati?", o_nc.v_Nume, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            o_nc.v_NumarRepetari -= 1;
                            if (o_nc.v_NumarRepetari == 0) o_nc.v_Repetare = false; //instant disable it


                            if (o_nc.v_ModRepetare == "Lunar")
                            {
                                o_nc.v_Lansare = o_nc.v_Lansare.Date.AddMonths(1);
                            }
                            else if (o_nc.v_ModRepetare == "Zilnic")
                            {
                                o_nc.v_Lansare = o_nc.v_Lansare.Date.AddDays(1);
                            }
                            else
                            {
                                o_nc.v_Lansare = o_nc.v_Lansare.Date.AddDays(7);
                            }
                            o_nc.v_UltimaNotif = DateTime.Now.Date;
                        }
                    }
                    else
                    {
                        o_nc.v_Repetare = false;
                    }
                }
                passed_LSystem.o_SQLManager.Insert(nc);
            }
        }

        private void OnTodayNotesClicked(object sender, EventArgs e)
        {
            showTodayNotes();
        }
        private void OnMenuClicked(object sender, EventArgs e)
        {
            FormularGridView x = FormularGridView.GetInstance(this.passed_LSystem);
            x.Show();
        }
        private void OnOptionsClicked(object sender, EventArgs e)
        {
            FormularOptiuni x = FormularOptiuni.GetInstance(this.passed_LSystem);
            x.Show();
        }
        private void OnExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void Dispose()
        {
            if (o_Timer != null) { o_Timer.Dispose(); o_Timer = null; }
            if (o_NotifyIcon != null) { o_NotifyIcon.Dispose(); o_NotifyIcon = null; }
            if (o_ContextMenu != null) { o_ContextMenu.Dispose(); o_ContextMenu = null; }

        }
    }
}
